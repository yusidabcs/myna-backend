<?php namespace App\Transformer;

use Facebook;

use League\Fractal\TransformerAbstract;

class FacebookTransformer extends TransformerAbstract
{
    protected $availableEmbeds = [
        'user'
    ];

    /**
     * Turn this item object into a generic array
     *
     * @return array
     */
    public function transform(Facebook $fb)
    {
        return [
            'id'             => (int) $fb->id,
            'fbUsername'     => $fb->fbUsername,
            'fbUserId'       => $fb->fbUserId,
            'fbUserAccessToken'       => $fb->fbUserAccessToken,
            'userId'         => $fb->userId,
            'created_at'     => $fb->created_at,
            'updated_at'     => $fb->updated_at,
        ];
    }

    public function embedUser(Facebook $fb)
    {
        $user = $fb->user;
        return $this->item($user, new UserTrasnformer);
    }
}