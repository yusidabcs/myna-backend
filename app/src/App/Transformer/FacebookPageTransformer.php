<?php namespace App\Transformer;

use FacebookPage;

use League\Fractal\TransformerAbstract;

class FacebookPageTransformer extends TransformerAbstract
{
    protected $availableEmbeds = [
        'facebook',
        'user'
    ];

    /**
     * Turn this item object into a generic array
     *
     * @return array
     */
    public function transform(FacebookPage $fb)
    {
        return [
            'id'             => (int) $fb->id,
            'fbPageUsername'     => $fb->fbPageUsername,
            'fbPageAccessToken'       => $fb->fbPageAccessToken,
            'fbPageUserId'         => $fb->fbPageUserId,
            'fbPageId'         => $fb->fbPageId,        
            'created_at'     => $fb->created_at,
            'updated_at'     => $fb->updated_at,
        ];
    }
    public function embedFacebook(FacebookPage $fb)
    {
        $facebook = $fb->facebook;
        return $this->collection($facebook, new FacebookTransformer);
    }
    public function embedFacebook(FacebookPage $fb)
    {
        $user = $fb->user;
        return $this->collection($user, new UserTransformer);
    }
}