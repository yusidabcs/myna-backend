<?php namespace App\Transformer;

use Post;

use League\Fractal\TransformerAbstract;

class PostTransformer extends TransformerAbstract
{
    protected $availableEmbeds = [        
        'user',
        'facebooks',
        'facebookpages',
        'twitters'
    ];
    /**
     * Turn this item object into a generic array
     *
     * @return array
     */
    public function transform(Post $post)
    {
        return [
            'id'             => (int) $post->id,
            'content'     => $post->content,
            'photo'       => $post->photo,
            'schedule'         => $post->schedule,
            'routine'     => $post->routine,
            'email'     => $post->email,
            'userId'     => $post->userId,
            'routineTypeId'     => $post->routineTypeId,            
            'created_at'     => $post->created_at,
            'updated_at'     => $post->updated_at,
        ];
    }
    public function embedUser(Post $post)
    {
        $user = $post->user;
        return $this->item($user, new UserTransformer);
    }
    public function embedFacebooks(Post $post)
    {
        $fb = $post->facebooks;
        return $this->collection($fb, new FacebookTransformer);
    }
    public function embedFacebookpages(Post $post)
    {
        $fb = $post->facebookpages;
        return $this->collection($fb, new FacebookPageTransformer);
    }
    public function embedTwitters(Post $post)
    {
        $tw = $post->twitters;
        return $this->collection($tw, new TwitterTransformer);
    }
}