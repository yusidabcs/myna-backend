<?php namespace App\Transformer;

use Images;

use League\Fractal\TransformerAbstract;

class ImageTransformer extends TransformerAbstract
{
    protected $availableEmbeds = [        
        'user'
    ];

    /**
     * Turn this item object into a generic array
     *
     * @return array
     */
    public function transform(Images $image)
    {
        return [
            'id'             => (int) $image->id,
            'image'     => $image->image,
            'thumb'     => $image->thumb,
            'userId'       => $image->userId,
            'created_at'     => $image->created_at,
            'updated_at'     => $image->updated_at,
        ];
    }
    public function embedUsers(Images $image)
    {
        $user = $image->user;
        return $this->item($user, new UserTransformer);
    }
}