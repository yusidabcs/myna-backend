<?php namespace App\Transformer;

use Twitter;

use League\Fractal\TransformerAbstract;

class TwitterTransformer extends TransformerAbstract
{
    protected $availableEmbeds = [
        'user'
    ];

    /**
     * Turn this item object into a generic array
     *
     * @return array
     */
    public function transform(Twitter $tw)
    {
        return [
            'id'             => (int) $tw->id,
            'twUsername'     => $tw->twUsername,
            'twAccessToken'       => $tw->twAccessToken,
            'twTokenSecret'         => $tw->twTokenSecret,
            'twConsumerKey'     => $tw->twConsumerKey,
            'twConsumerSecret'     => $tw->twConsumerSecret,
            'created_at'     => $tw->created_at,
            'updated_at'     => $tw->updated_at,
        ];
    }

    public function embedUser(Twitter $tw)
    {
        $user = $tw->user;
        return $this->item($user, new UserTrasnformer);
    }
}