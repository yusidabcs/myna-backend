<?php namespace App\Transformer;

use User;

use League\Fractal\TransformerAbstract;

class UserTransformer extends TransformerAbstract
{
    protected $availableEmbeds = [        
        'facebooks',
        'posts'
    ];

    /**
     * Turn this item object into a generic array
     *
     * @return array
     */
    public function transform(User $user)
    {
        return [
            'id'             => (int) $user->id,
            'firstName'     => $user->firstName,
            'lastName'       => $user->lastName,
            'email'         => $user->email,
            'ipAddress'     => $user->ipAddress,
            'telp'     => $user->telp,
            'plan'     => $user->plan,
            'billingType'     => $user->billingType,
            'postPerDayLimit'     => $user->postPerDayLimit,
            'maxAccount'     => $user->maxAccount,
            'maxAutoPilot'     => $user->maxAutoPilot,
            'timeZone'     => $user->timeZone,
            'currency'     => $user->currency,
            'language'     => $user->language,
            'emailNotification'     => $user->emailNotification,
            'schedulePostNotif'     => $user->schedulePostNotif,
            'routinePostNotif'     => $user->routinePostNotif,
            'autopilotPostNotif'     => $user->autopilotPostNotif,
            'created_at'     => $user->created_at,
            'updated_at'     => $user->updated_at,
        ];
    }

    public function embedFacebooks(User $user)
    {
        $facebooks = $user->facebooks;
        return $this->collection($facebooks, new FacebookTransformer);
    }
    public function embedPosts(User $user)
    {
        $post = $user->posts;
        return $this->collection($post, new PostTransformer);
    }
}