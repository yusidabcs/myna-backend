<?php namespace App\Transformer;

use Scheduledpush;

use League\Fractal\TransformerAbstract;

class ScheduleTransformer extends TransformerAbstract
{
    protected $availableEmbeds = [        
        'post',
        'product',
        'user'
    ];
    /**
     * Turn this item object into a generic array
     *
     * @return array
     */
    public function transform(Scheduledpush $sc)
    {
        return [
            'id'             => (int) $sc->id,
            'pushDate'     => $sc->pushDate,
            'status'       => $sc->status,
            'note'         => $sc->note,
            'pushId'     => $sc->pushId,
            'postId'     => $sc->postId,
            'productId'     => $sc->productId,
            'userId'     => $sc->userId,            
            'created_at'     => $sc->created_at,
            'updated_at'     => $sc->updated_at,
        ];
    }
    public function embedUser(Scheduledpush $sc)
    {
        $user = $sc->user;
        return $this->item($user, new UserTransformer);
    }
    public function embedPost(Scheduledpush $sc)
    {
        $post = $sc->post;
        if($post!=null)
            return $this->item($post, new PostTransformer);
    }
    public function embedProduct(Scheduledpush $sc)
    {
        $product = $sc->product;
        if($product!=null)
            return $this->item($product, new ProductTransformer);
    }    
}