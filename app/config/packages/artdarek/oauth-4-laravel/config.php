<?php 

return array( 
	
	/*
	|--------------------------------------------------------------------------
	| oAuth Config
	|--------------------------------------------------------------------------
	*/

	/**
	 * Storage
	 */
	'storage' => 'Session', 

	/**
	 * Consumers
	 */
	'consumers' => array(

		/**
		 * Facebook
		 */
        'Facebook' => array(
            'client_id'     => '1503461186544969',
            'client_secret' => 'e7ce7c4e0396b990f10bc7272ae8e15c',
            'fileUpload' => true,
    		'allowSignedRequest' => false 
        ),		

	)

);