<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateFacebooksTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('facebooks', function(Blueprint $table) {
			$table->bigIncrements('id');
			$table->string('fbUsername');
			$table->string('fbUserAccessToken');
			$table->bigInteger('fbUserId');
			$table->integer('userId');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('facebooks');
	}

}
