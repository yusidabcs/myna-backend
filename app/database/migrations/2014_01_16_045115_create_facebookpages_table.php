<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateFacebookpagesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('facebookpages', function(Blueprint $table) {
			$table->increments('id');
			$table->string('fbPageUsername');
			$table->string('fbPageAccessToken');
			$table->string('fbPageUserId');
			$table->string('fbPageId');
			$table->integer('facebookId');
			$table->integer('userId');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('facebookpages');
	}

}
