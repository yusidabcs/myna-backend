<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateUsersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('users', function(Blueprint $table) {
			$table->increments('id');
			$table->string('firstName');
			$table->string('lastName');
			$table->string('email');
			$table->string('password');
			$table->string('ipAddress',15);
			$table->string('telp',45);
			$table->dateTime('activationDate');
			$table->string('plan',45);
			$table->string('securityCode');
			$table->string('billingType');
			$table->integer('postPerDayLimit');
			$table->integer('maxAccount');
			$table->integer('maxAutopilot');
			$table->string('timeZone',100);
			$table->string('currency',45);
			$table->string('language',45);
			$table->string('emailNotification');
			$table->integer('schedulePostNotif');
			$table->integer('routinePostNotif');
			$table->integer('autopilotPostNotif');
			$table->timestamps();
		});
	}
	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('users');
	}

}
