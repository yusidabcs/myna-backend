<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateProductsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('products', function(Blueprint $table) {
			$table->increments('id');
			$table->text('postTo');
			$table->text('productName');
			$table->integer('price');
			$table->text('description');
			$table->text('howToBuy');
			$table->string('photo');
			$table->dateTime('schedule');
			$table->string('routine');
			$table->integer('outopilot');
			$table->string('targetGender');
			$table->integer('targetAgeMin');
			$table->integer('targetAgeMax');
			$table->string('targetLocation');
			$table->string('email');
			$table->integer('status');
			$table->integer('routineTypeId');
			$table->integer('categoryId');
			$table->integer('userId');;
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('products');
	}

}
