<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateScheduledpushesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('scheduledpushes', function(Blueprint $table) {
			$table->increments('id');
			$table->dateTime('pushDate');
			$table->integer('status');
			$table->text('note');		
			$table->integer('confirmation');		
			$table->string('pushId',32);
			$table->integer('postId')->nullable();
			$table->integer('productId')->nullable();
			$table->integer('userId');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('scheduledpushes');
	}

}
