<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateOutopilotpushesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('outopilotpushes', function(Blueprint $table) {
			$table->increments('id');
			$table->dateTime('pushDate');
			$table->integer('status');
			$table->text('note');			
			$table->integer('pushId');
			$table->integer('productId');
			$table->integer('userId');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('outopilotpushes');
	}

}
