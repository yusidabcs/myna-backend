<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTwittersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('twitters', function(Blueprint $table) {
			$table->increments('id');
			$table->string('twUsername');
			$table->string('twAccessToken');
			$table->string('twTokenSecret');
			$table->string('twConsumerKey');
			$table->string('twConsumerSecret');
			$table->string('bestDay');
			$table->string('bestDayTime');
			$table->string('bestTime');			
			$table->string('userId');			
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('twitters');
	}

}
