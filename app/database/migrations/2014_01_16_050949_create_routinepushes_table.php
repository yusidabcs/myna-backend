<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateRoutinepushesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('routinepushes', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('date');
			$table->integer('day');
			$table->time('time');
			$table->integer('status');
			$table->text('note');			
			$table->integer('postId');
			$table->integer('productId');
			$table->integer('userId');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('routinepushes');
	}

}
