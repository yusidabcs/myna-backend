<?php

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Eloquent::unguard();

		// $this->call('UserTableSeeder');
		$this->call('CommentsTableSeeder');
		$this->call('PostsTableSeeder');
		$this->call('CategoriesTableSeeder');
		$this->call('UsersTableSeeder');
		$this->call('FacebooksTableSeeder');
		$this->call('FacebookpagesTableSeeder');
		$this->call('ProductsTableSeeder');
		$this->call('RoutinetypesTableSeeder');
		$this->call('EventsTableSeeder');
		$this->call('TwittersTableSeeder');
		$this->call('ScheduledpushesTableSeeder');
		$this->call('RoutinepushesTableSeeder');
		$this->call('OutopilotpushesTableSeeder');
		$this->call('TimelinesTableSeeder');
		$this->call('ImagesTableSeeder');
		$this->call('TransactionsTableSeeder');
	}

}