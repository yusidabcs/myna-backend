<?php

class UsersTableSeeder extends Seeder {

	public function run()
	{
		// Uncomment the below to wipe the table clean before populating
		// DB::table('users')->truncate();

		$users = array(
			'email'=>'agusyusida@gmail.com',
			'password'=>Hash::make('admin'),
			'firstName' =>'agus',
			'lastName' => 'yusida'
		);

		// Uncomment the below to run the seeder
		DB::table('users')->insert($users);
	}

}
