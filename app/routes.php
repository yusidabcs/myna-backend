<?php
/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/
Route::group(array('namespace' => 'Frontend'), function()
{
	Route::get('/','IndexController@index');
	Route::get('/home','IndexController@index');
	Route::get('/pricing','PricingController@index');
	Route::get('/teams','TeamController@index');
	Route::get('/faq','FaqController@index');
	Route::get('/tos','TosController@index');
	Route::get('/contact','ContactController@index');
	Route::get('/feature','FeatureController@index');	
	Route::group(array('before' => 'crsf'), function(){
		Route::post('/register','RegisterController@index');
	});
});

Route::group(array('prefix'=>'apiv1'), function()
{  
	header('Access-Control-Allow-Origin : http://www.myna.local:9000');
	header('Access-Control-Allow-Credentials : true');
	header('Access-Control-Allow-Methods: OPTIONS, POST, GET, PUT, DELETE');
	header("Access-Control-Allow-Headers: Origin, cache-control, X-Requested-With, Content-Type, Accept,Authorization,Crsf");	
	Route::group(array('before' => 'auth.basic'), function()
	{		
		Route::resource('users', 'UsersController');		
		Route::resource('products', 'ProductsController');
		Route::resource('categories', 'CategoriesController');
		Route::resource('routinetypes', 'RoutinetypesController');
		Route::resource('events', 'EventsController');
		Route::resource('twitters', 'TwittersController');
		Route::resource('scheduledpushes', 'ScheduledpushesController');
		Route::resource('routinepushes', 'RoutinepushesController');
		Route::resource('outopilotpushes', 'OutopilotpushesController');
		Route::resource('timelines', 'TimelinesController');
		Route::resource('transactions', 'TransactionsController');
		
		//exteding route
		Route::resource('users/{id}/facebooks', 'FacebooksController');
		Route::resource('users/{id}/facebookpages', 'FacebookpagesController');
		Route::resource('users/{id}/images', 'ImagesController');
		Route::resource('users/{id}/posts', 'PostsController');
		Route::resource('users/{id}/twitters', 'TwittersController');
		Route::resource('users/{id}/scheduledpushes', 'ScheduledpushesController');

		Route::get('/logout', function()
		{		
			$log = Auth::logout();
			return Response::json($log);
		});		

		//process queue			
		Route::group(array('prefix'=>'queue'), function()
		{
			Route::get('posts/{id}',function($id){
				
				$post = Post::find($id);
				$fb = $post->facebooks;
				foreach ($fb as $key => $value) {
					if($post->photo!='')
					{
						Queue::push(function($job) use ($post,$value)
						{				
							FB::setAccessToken($value->fbUserAccessToken);
							$fb = FB::api($value->fbUserId.'/photos', 'POST', array(
                                     'url' => $post->photo,
                                     'message' => $post->content,
                                     )
                                  );
							$job->delete();
						});
					}
					else
					{
						Queue::push(function($job) use ($post,$value)
						{			
							FB::setAccessToken($value->fbUserAccessToken);	
							$fb = FB::api($value->fbUserId.'/feed','POST',array(	        
					        	'message' => $post->content,
					    	));
							$job->delete();
						});
					}	
				}

				//check schedule
				$scheduleId = Input::get('scheduledId');
				if($scheduleId!=0)
				{
					$sc = Scheduledpush::find($scheduleId);
					$worker = new IronWorker(Config::get('iron'));									
					$schedule = $worker->getSchedule($sc->pushId);
					if($schedule->status=='complete')
					{
						$sc->status=1;
						$sc->save();

						if($post->scheduled->confirmation==1){
							//send email
						}
					}
				}
			});
		});		

		//process schedule
		Route::group(array('prefix'=>'schedule'), function()
		{
			Route::get('posts/{id}',function($id){				
				$post = Post::find($id);
				$fb = $post->facebooks;
				foreach ($fb as $key => $value) {
					if($post->photo!='')
					{
						Queue::push(function($job) use ($post,$value)
						{				
							FB::setAccessToken($value->fbUserAccessToken);
							$fb = FB::api($value->fbUserId.'/photos', 'POST', array(
                                     'url' => $post->photo,
                                     'message' => $post->content,
                                     )
                                  );
							$job->delete();
						});
					}
					else
					{
						Queue::push(function($job) use ($post,$value)
						{			
							FB::setAccessToken($value->fbUserAccessToken);	
							$fb = FB::api($value->fbUserId.'/feed','POST',array(	        
					        	'message' => $post->content,
					    	));
							$job->delete();
						});
					}					
				}

				//check status ironworker
				$worker = new IronWorker(Config::get('iron'));				
				$scheduled = $worker->getTaskDetails($post->scheduled->pushId);

				//update status when done
				if($scheduled->status=='complete')
				{
					$post->scheduled->status=1;
					$post->scheduled->save();

					if($post->scheduled->confirmation==1){
						//send email
					}
				}

			});
		});
	});
	Route::group(array('prefix'=>'apiv1/routine'), function()
	{
		Route::get('posts',function(){
			//select tanggal hari ini		
			$now = date('r');
			$now = strtotime($now);

			$users = User::all();				
			foreach ($users as $key => $user) {
				$routine = $user->routines()->where(function($query){						
					$date = date('j');
					$day = date('N');					 	
				 	$query->where('date','=',$date)->orWhere('day','=',$day);
				})->orWhere(function($query){					 
				 	$query->where('day','=',0)->where('date','=',0);											 	
				})->get();

				if($routine->count()>0){
					//push queue
					Queue::push(function($job) use ($routine,$user)
					{			
						//set timezone user
						date_default_timezone_set('Asia/Jakarta');
						foreach ($routine as $key => $value) {
							//insert new schedule based routine
							$scheduled = new Scheduledpush;
							$scheduled->pushDate = date('Y-m-d '.$value->time);
							$scheduled->status = 0;
							$scheduled->note = 'schedule from routine.';
							$scheduled->confirmation = 0;
							$scheduled->userId = $value->userId;
							$scheduled->postId = $value->postId;
							$scheduled->save();

							$now = date('r');
							$now = strtotime($now);
							$time = strtotime($scheduled->pushDate);			
							$now = (int)$now;
							$future = (int)$time;
							$start_at = ($future-$now);
							$worker = new IronWorker(Config::get('iron'));												
							$pushId = $worker->postScheduleSimple('ScheduleWorker',array(
										'postId'=>$value->postId,
										'scheduledId'=>$scheduled->id,
										'authorization'=>Request::header('Authorization')
										), $start_at);

							$scheduled->pushId = $pushId;
							$scheduled->save();
						}
						$job->delete();
					});
				}												

			}					
		});
	});
	Route::resource('login','LoginController');
	Route::options('{name}', function($name){})->where('name', '[A-Za-z0-9]+');
	Route::options('{name}/{name2}', function($name,$name2){})->where('name', '[A-Za-z0-9]+')->where('name2', '[0-9]+');
	Route::options('{name}/{name2}/{name3}', function($name,$name2){})->where('name', '[A-Za-z0-9]+')->where('name2', '[0-9]+')->where('name3', '[A-Za-z0-9]+');
	Route::options('{name}/{name2}/{name3}/{name4}', function($name,$name2){})->where('name', '[A-Za-z0-9]+')->where('name2', '[0-9]+')->where('name3', '[A-Za-z0-9]+');
	
});
Route::get('/tes',function(){
	return User::all();
	$client = new Client('http://mycode.eu1.frbit.net');
	$request = $client->get('/tes2');
	$response = $request->send();
	return $response;
});
Route::get('/tes2',function(){	
	//set timezone
	$now = date('r');
	$now = strtotime($now);

	$futureDate = $now+(60*1);
	$time = strtotime($futureDate);
	$futureDate = date($futureDate);

	$now = (int)$now;
	$future = (int)$futureDate;
	$start_at = ($future-$now);

	$worker = new IronWorker(Config::get('iron'));
	// run the task in five minutes time (given in seconds)
	
	$schedID = $worker->postScheduleSimple('myworker',array('message'=>'Apalah salah gw lagi ne. sialan ah looo!'), $start_at);
	echo $start_at;
	//$taskID = $worker->postTask('');
});

Route::get('queue/send',function(){
	$post = Post::find(30);
	$fb = $post->facebooks;
	return $post;
	$data = 'tes tes';
	Queue::push(function($job) use ($data) 
	{
			  
		File::append(app_path().'/queue.txt',$data.PHP_EOL);
		$job->delete();

	});
	return "OK";
});

Route::post('queue/push',function(){
	Return Queue::marshall();
});


