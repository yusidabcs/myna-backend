<?php
class Twitter extends Eloquent {
	
	protected $guarded = array();
	public static $rules = array();
	protected $table = "twitters";

	public function posts(){
		return $this->belongsToMany('Post','postId');
	}
	public function user(){
		return $this->belongsTo('User','userId');
	}
}
