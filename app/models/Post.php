<?php
class Post extends Eloquent{
	protected $table = 'posts';
    public function user()
    {
        return $this->belongsTo('User','userId');
    }
    public function facebooks()
    {
    	return $this->belongsToMany('Facebook','transactions','postId','facebookId');
    }
    public function facebookpages()
    {
    	return $this->belongsToMany('Facebookpage','transactions','postId','facebookPageId');
    }
    public function twitters()
    {
    	return $this->belongsToMany('Twitter','transactions','postId','twitterId');
    }
    public function scheduled()
    {
        return $this->hasMany('Scheduledpush','postId');
    }
}