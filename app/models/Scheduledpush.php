<?php

class Scheduledpush extends Eloquent {
	protected $guarded = array();
	public static $rules = array();
	protected $table = 'scheduledpushes';
	
	public function post(){
		return $this->belongsTo('Post','postId');
	}

	public function product(){
		return $this->belongsTo('Product','productId');
	}

	public function user(){
		return $this->belongsTo('Post','userId');
	}
}
