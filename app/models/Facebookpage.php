<?php

class Facebookpage extends Eloquent {
	protected $guarded = array();
	public static $rules = array();
	public function facebook(){
		return $this->belongsTo('Facebook','facebookId');
	}
	public function user(){
		return $this->belongsTo('User','userId');
	}
}
