<?php

class Routinepush extends Eloquent {
	protected $guarded = array();
	public static $rules = array();

	public function post()
	{
		return $this->belongsTo('Post','postId');
	}
	public function product()
	{
		return $this->belongsTo('Product','productId');
	}
}
