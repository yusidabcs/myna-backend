<?php

class Category extends Eloquent {
	protected $fillable = array('name');
	protected $table = 'categories';
	public function product()
    {
        return $this->hasMany('Product','categoryId');
    }
}
