<?php

class Facebook extends Eloquent {
	protected $guarded = array();
	public static $rules = array();
	public function posts(){
		return $this->belongsToMany('Post','postId');
	}
	public function user(){
		return $this->belongsTo('User','userId');
	}
}
