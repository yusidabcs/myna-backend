<!-- <!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8" />
  <title>Dashboard - Ace Admin</title>

  <meta name="description" content="overview &amp; stats" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />

  <!--basic styles-->

  <link href="assets/css/bootstrap.min.css" rel="stylesheet" />
  <link href="assets/css/bootstrap-responsive.min.css" rel="stylesheet" />
  <link rel="stylesheet" href="assets/css/font-awesome.min.css" />
  <link rel="stylesheet" href="assets/css/redactor.css" />    
  <link rel="stylesheet" href="assets/css/dropzone.css" />    
  <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Open+Sans:400,300" />
  <link rel="stylesheet" href="assets/css/ace.min.css" />
  <link rel="stylesheet" href="assets/css/ace-responsive.min.css" />
  <link rel="stylesheet" href="assets/css/ace-skins.min.css" />
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
</head>
<body style="" data-ember-extension="1" class="navbar-fixed breadcrumbs-fixed">
<script type="text/x-handlebars" data-template-name="application">
{{#if App.isLogin}}
  {{#if App.isLoading}}
  <div style="left:49%;top:8px;position:fixed;z-index:99999">
    <i class="icon-refresh icon-spin white bigger-210"></i>
  </div>
  {{/if}}
  <div class="navbar navbar-fixed-top">
    <div class="navbar-inner">
      <div class="container-fluid">
        <a href="#" class="brand">
          <small>
            <i class="icon-leaf"></i>
            Ace Admin
          </small>
        </a><!--/.brand-->    
        {{view App.topbarView valueBinding="user"}}                
      </div><!--/.container-fluid-->
    </div><!--/.navbar-inner-->
  </div>

  <div class="main-container container-fluid">
    <a class="menu-toggler" id="menu-toggler" href="#">
      <span class="menu-text"></span>
    </a>

    <div class="sidebar fixed" id="sidebar">
      <div class="sidebar-shortcuts" id="sidebar-shortcuts">
        <div class="sidebar-shortcuts-large" id="sidebar-shortcuts-large">
          <button class="btn btn-small btn-success">
            <i class="icon-signal"></i>
          </button>

          <button class="btn btn-small btn-info">
            <i class="icon-pencil"></i>
          </button>

          <button class="btn btn-small btn-warning">
            <i class="icon-group"></i>
          </button>

          <button class="btn btn-small btn-danger">
            <i class="icon-cogs"></i>
          </button>
        </div>

        <div class="sidebar-shortcuts-mini" id="sidebar-shortcuts-mini">
          <span class="btn btn-success"></span>

          <span class="btn btn-info"></span>

          <span class="btn btn-warning"></span>

          <span class="btn btn-danger"></span>
        </div>
      </div><!--#sidebar-shortcuts-->

      {{view App.navbarView}}

      <div class="sidebar-collapse" id="sidebar-collapse">
        <i class="icon-double-angle-left"></i>
      </div>
    </div>

    <div class="main-content">

      {{outlet}}      

    </div><!--/.main-content-->
  </div><!--/.main-container-->
{{else}}
{{outlet}}
{{/if}}
</script>
<script type="text/x-handlebars" data-template-name="category/empty">
</script>
<script type="text/x-handlebars" data-template-name="dropzone">
<div id="myImage">
  <div id="gambarplace" style="z-index:10;position:relative">               
    <div class="row-fluid">
      {{#each view.listImageArray}}
      <div class="span2 well">                    
        <center><img src="images/thumb/{{this}}" style="width:75px;height:75px"></center>
          <br>
          <center>
            <a class="btn btn-mini btn-danger" style="cursor:pointer" title="delete" {{action deleteImage this on="click" target="view"}}>
              <i class="icon-trash icon-only"></i>
            </a>
            <a class="btn btn-mini btn-success" style="cursor:pointer" title="copy link" {{action copyImage this on="click" target="view"}}>
              <i class="icon-share-alt"></i>
            </a>
          </center>
      </div>
      {{/each}}
    </div>
  </div>
</div>
</script>


  <script type="text/x-handlebars" data-template-name="modal_layout">
    <div class="modal-backdrop fade">&nbsp;</div>  
    <div class="modal fade show" >
      <div class="modal-dialog">
        <div class="modal-content">
          {{yield}}
        </div>
      </div>
    </div>
  </script> 
  <script type="text/x-handlebars" data-template-name="pagination">
    {{#with view}}
    {{#linkTo 'posts.page' page}}
    {{content.page_id}}
    {{/linkTo}}
    {{/with}}
  </script>
  <script type="text/x-handlebars" data-template-name="navbar">
    <ul class="nav nav-list">      
      <li >
        <a href="#/dashboard">
          <i class="icon-dashboard"></i>
          <span class="menu-text">Dashboard </span>
        </a>
      </li>
      <li >
        <a href="#" class="dropdown-toggle" target="_self">
          <i class="icon-desktop"></i>
          <span class="menu-text">Post </span>
          <b class="arrow icon-angle-down"></b>
        </a>

        <ul class="submenu">
          <li >
            <a href="#/posts">
              <i class="icon-double-angle-right"></i>
              List Post
            </a>
          </li>
          <li >
            <a href="#/posts/create">
              <i class="icon-double-angle-right"></i>
              Create Post
            </a>
          </li>
        </ul>
      </li>
      <li >
        <a href="#/category">
          <i class="icon-hdd"></i>
          <span class="menu-text">Category </span>
        </a>
      </li>
    </ul>
  </script>
  <script type="text/x-handlebars" data-template-name="topbar">
    <ul class="nav ace-nav pull-right">
            <li class="green">
              <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                <i class="icon-envelope icon-animated-vertical"></i>
                <span class="badge badge-success">5</span>
              </a>

              <ul class="pull-right dropdown-navbar dropdown-menu dropdown-caret dropdown-closer">
                <li class="nav-header">
                  <i class="icon-envelope-alt"></i>
                  5 Messages
                </li>
              </ul>
            </li>

            <li class="light-blue">
              <a data-toggle="dropdown" href="#" target="_self" class="dropdown-toggle">
                <img class="nav-user-photo" src="assets/avatars/user.jpg" alt="Jason's Photo">
                <span class="user-info">
                  <small>Welcome,</small>
                  {{user.first_name}} {{user.last_name}}
                </span>

                <i class="icon-caret-down"></i>
              </a>

              <ul class="user-menu pull-right dropdown-menu dropdown-yellow dropdown-caret dropdown-closer">
                <li>
                  <a href="#">
                    <i class="icon-cog"></i>
                    Settings
                  </a>
                </li>

                <li>
                  <a href="#">
                    <i class="icon-user"></i>
                    Profile
                  </a>
                </li>

                <li class="divider"></li>

                <li>
                  <a href="#" {{action 'logout'}}>
                    <i class="icon-off"></i>
                    Logout
                  </a>
                </li>
              </ul>
            </li>
          </ul><!--/.ace-nav-->
  </script>
  <script type="text/x-handlebars" data-template-name="create_category">
    <div class="bootbox modal fade" tabindex="-1" style="overflow:hidden;" id="modal">
      <div class="modal-header">
        <a href="#" class="close" {{action close}}>×</a>
        <h3>Create Category</h3>
      </div>
    <div class="modal-body">
      <div class="row-fluid">
        <div class="span12">
          <!--PAGE CONTENT BEGINS-->

          <form class="form-horizontal">
            <div class="control-group">
            <label class="control-label" for="form-field-1">Title</label>

              <div class="controls">
                <input type="text" id="form-field-1" placeholder="Username">
              </div>
            </div>

            <div class="control-group">
              <label class="control-label" for="form-field-2">Parent</label>
              <div class="controls">
                 {{view Ember.Select content=listCategory optionValuePath="content.id" optionLabelPath="content.title" prompt="Main Parent" 
                       selectionBinding="parent"
                       }} 
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
    <div class="modal-footer">
      <a data-handler="0" class="btn null" href="#" {{action close}}>Cancel</a>
      <a data-handler="1" class="btn btn-primary" href="#" {{action save}}>Save</a>
    </div>
  </div>
  </script>
  <!--basic scripts-->
  <script src="js/libs/jquery-1.10.2.js"></script>    
  <script src="assets/js/ace-elements.min.js"></script>
  <script src="assets/js/ace.min.js"></script>
  <script src="assets/js/jquery.dataTables.min.js"></script>
  <script src="assets/js/jquery.dataTables.bootstrap.js"></script>
  <script src="assets/js/redactor.min.js"></script>   
  <script src="assets/js/dropzone.js"></script>
  <script src="assets/js/bootstrap.min.js"></script>

    <script src="js/libs/handlebars-1.1.2.js"></script>
    <script src="js/libs/ember-1.2.0.js"></script>
    <script src="//builds.emberjs.com/tags/v1.0.0-beta.3/ember-data.js"></script>
    <script src="http://cdnjs.cloudflare.com/ajax/libs/showdown/0.3.1/showdown.min.js"></script>
    <script src="http://cdnjs.cloudflare.com/ajax/libs/moment.js/2.1.0/moment.min.js"></script>
    <script src="js/libs/bs-core.min.js"></script>
    <script src="js/libs/bs-modal.min.js"></script>
    <script src="js/libs/bs-button.min.js"></script>
    <script src="js/app.js"></script>
    <script src="js/model.js"></script>
    <script src="js/router.js"></script>
  <!--inline scripts related to this page-->  
</body>
</html>
 -->