@section('content')
<section class="block-large">
	<div class="container">
		<div class="row-fluid">
			<div class="span7" align="right">
				<img src="assets/images/social-media.png" width="400" style="margin-right:70px;">
			</div>
			<div class="span5">
				<br class="spacer-xlarge">
				<h1>myna.io</h1>
				<h3>Internet marketing, automated.</h3>
				<br><br>
				<h3 class="md-trigger" data-modal="modal-1"><a href="#">Register Now &nbsp;&nbsp;&nbsp;&nbsp;&rang;</a></h3>

			</div>
		</div><!-- /.row -->
	</div><!-- /.container -->
</section><!-- /.teaser -->

<div class="md-modal md-effect-8" id="modal-1">
	<div class="md-content">
		<h3>Register</h3>
		<div>
			<center>
				<input class="input input-block-level" type="text" placeholder="Name">
				<br>
				<input class="input input-block-level" type="text" placeholder="Email">
				<br>
				<input class="input input-block-level" type="text" placeholder="Password">
				<br>
				<small>I have read and agree to the <a href="#">Terms of Use</a></small>
				<br><br>
				<input type="submit" class="btn" value="Create Account">

				<hr>
				or<br><br>
				<button class="btn btn-info">Register with Facebook</button>
			</center>
		</div>
	</div>
</div>
@endsection