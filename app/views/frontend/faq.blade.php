@section('content')
<section class="block-large" style="padding-top:20px;">
		<center>
			<h2>F.A.Q</h2>
		</center>
		<hr>
		<div class="container">
			<div class="row">
				<div class="span12">
					<div class="row-fluid">
						<div class="span12">
							<div class="accordion" id="accordion2">
					        <div class="accordion-group" style="background-color:#fff;">
					          <div class="accordion-heading">
					            <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne">
					              Pengisian/Input Produk
					            </a>
					          </div>
					          <div id="collapseOne" class="accordion-body collapse">
					            <div class="accordion-inner">
					            
					              Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of "de Finibus Bonorum et Malorum" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, "Lorem ipsum dolor sit amet..", comes from a line in section 1.10.32.
					              
					              
					            </div>
					          </div>
					        </div>
					        <div class="accordion-group" style="background-color:#fff;">
					          <div class="accordion-heading">
					            <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseTwo">
					              Proses Order
					            </a>
					          </div>
					          <div id="collapseTwo" class="accordion-body collapse">
					            <div class="accordion-inner">
					             
					            Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of "de Finibus Bonorum et Malorum" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, "Lorem ipsum dolor sit amet..", comes from a line in section 1.10.32.
					             
					             	          	          
					            </div>
					          </div>
					        </div>
					        
					        <div class="accordion-group" style="background-color:#fff;">
					          <div class="accordion-heading">
					            <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseThree">
					              Status Order
					            </a>
					          </div>
					          <div id="collapseThree" class="accordion-body collapse">
					            <div class="accordion-inner">
					                 Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of "de Finibus Bonorum et Malorum" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, "Lorem ipsum dolor sit amet..", comes from a line in section 1.10.32.
					            </div>
					          </div>
					        </div>

					        <div class="accordion-group" style="background-color:#fff;">
					          <div class="accordion-heading">
					            <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseFour">
					              Koleksi Produk
					            </a>
					          </div>
					          <div id="collapseFour" class="accordion-body collapse">
					            <div class="accordion-inner">
					                Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of "de Finibus Bonorum et Malorum" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, "Lorem ipsum dolor sit amet..", comes from a line in section 1.10.32.
					            </div>
					          </div>
					        </div>

					        <div class="accordion-group" style="background-color:#fff;">
					          <div class="accordion-heading">
					            <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseFive">
					              Power-Ups
					            </a>
					          </div>
					          <div id="collapseFive" class="accordion-body collapse">
					            <div class="accordion-inner">
					                 Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of "de Finibus Bonorum et Malorum" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, "Lorem ipsum dolor sit amet..", comes from a line in section 1.10.32.
					            </div>
					          </div>
					        </div>

					      </div> <!-- end accordion -->

						</div>
						<!-- end accordion -->

					</div>
					
				</div><!-- /.span12 -->
			</div><!-- /.row -->
		</div>

		
	</section>
@endsection