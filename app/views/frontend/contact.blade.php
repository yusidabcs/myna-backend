@section('content')
<section class="block-large">
	<div class="container">
		<div class="row-fluid">
			<div class="span6" align="right">
					<img src="assets/images/service_1.jpg" width="400"><hr>
					<strong>Office</strong><br>
					Menara RnD Center 4th Floor<br>
					Jalan Gegerkalong Hilir No.47<br>
					Bandung Jawa Barat. Indonesia<br>
					<a href="mailto:hello@myna.io">hello@myna.io</a><br>
			</div>
			<div class="span6">
				<h2>Contact Us</h2>
				<hr>
				<form>
					<input type="text" class="input input-large" placeholder="Your Name"><br>
					<input type="text" class="input input-large" placeholder="Your Email"><br>
					<textarea class="input input-xlarge" placeholder="Your Message" style="height: 118px;"></textarea><br>
					<input type="submit" class="btn">
				</form>
			</div>
		</div><!-- /.row -->
	</div><!-- /.container -->
</section><!-- /.teaser -->
@endsection