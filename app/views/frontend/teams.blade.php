@section('content')
	<section class="block-medium">
		<center>
			<h2>Team</h2>
		</center>
		<hr>
		<div class="container">
			
			<div class="row">
				<div class="span4">
					<div class="plan plan-white">
						<br>
						<div class="plan-name">
							<img src="assets/images/user_1.jpg" class="img-circle" width="100">
						</div>
						<hr>
						<div class="plan-price">
							<h2><small>RF Moerdowo</small></h2>
						</div>
						<hr>
						<div class="plan-price" style="padding:0px 20px;">
							<p>Totox is a contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source.</p>
							
						</div>
						<hr>
						<div class="plan-price">
							<a href="#">github.com/totox777</a>
						</div>
						<br>
					</div><!-- /.plan -->
				</div><!-- /.span4 -->

				<div class="span4">
					<div class="plan plan-white">
						<br>
						<div class="plan-name">
							<img src="assets/images/user_3.jpg" class="img-circle" width="100">
						</div>
						<hr>
						<div class="plan-price">
							<h2><small>Yusida John</small></h2>
						</div>
						<hr>
						<div class="plan-price" style="padding:0px 20px;">
							<p>Yusida is a contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source.</p>
							
						</div>
						<hr>
						<div class="plan-price">
							<a href="#">facebook.com/yusida</a>
						</div>
						<br>
					</div><!-- /.plan -->
				</div><!-- /.span4 -->

				<div class="span4">
					<div class="plan plan-white">
						<br>
						<div class="plan-name">
							<img src="assets/images/user_2.jpg" class="img-circle" width="100">
						</div>
						<hr>
						<div class="plan-price">
							<h2><small>Gusindra Divanatha</small></h2>
						</div>
						<hr>
						<div class="plan-price" style="padding:0px 20px;">
							<p>Gusin is a contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source.</p>
							
						</div>
						<hr>
						<div class="plan-price">
							<a href="#">twitter.com/gusin44</a>
						</div>
						<br>
					</div><!-- /.plan -->
				</div><!-- /.span4 -->

				
			</div><!-- /.row -->
			<div class="text-center">
		</div>
		</div><!-- /.container -->
		
	</section>
@endsection