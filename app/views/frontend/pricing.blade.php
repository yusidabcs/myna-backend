
@section('content')
<section class="block-medium">
			<center>
				<h2>Choose your plan</h2>
			</center>
			<hr>
		<div class="container">
			<div class="row">
				<div class="span4">
					<div class="plan">
						<div class="plan-name">
							<h3>Basic</h3>
						</div>
						<div class="plan-price">
							<h2>FREE</h2>
						</div>
						<ul class="plan-features">
							<li><i class="icon-ok"></i> Unlimited Support <i class="icon-info-sign" data-toggle="tooltip" data-original-title="Jam Kerja"></i></li>
							<li><i class="icon-ok"></i> 5 Channel Account <i class="icon-info-sign" data-toggle="tooltip" data-original-title=".com .net .info .biz .org .us .name"></i></li>
							<li><i class="icon-ok"></i> 5 Autopilot Post</li>
							<li><i class="icon-ok"></i> Basic Report</li>
							<li><i class="icon-remove"></i> <span style="text-decoration:line-through">No Watermark
							</span></li>
							<li><i class="icon-remove"></i> <span style="text-decoration:line-through">Event Schedule</span></li>
							<li><i class="icon-remove"></i> <span style="text-decoration:line-through">Bulk Schedule</span></li>
						</ul>
						<div class="plan-cta">
							<a href="#" class="btn btn-large">Get Started</a>
						</div>
					</div><!-- /.plan -->
				</div><!-- /.span4 -->

				<div class="span4">
					<div class="plan">
						<div class="plan-name">
							<h3>Pro</h3>
						</div>
						<div class="plan-price">
							<h2><small>USD</small> 9.99 <small>/Mo</small></h2>
						</div>
						<ul class="plan-features">
							<li><i class="icon-ok"></i> Unlimited Support <i class="icon-info-sign" data-toggle="tooltip" data-original-title="Jam Kerja"></i></li>
							<li><i class="icon-ok"></i> 25 Channel Account <i class="icon-info-sign" data-toggle="tooltip" data-original-title=".com .net .info .biz .org .us .name"></i></li>
							<li><i class="icon-ok"></i> 25 Autopilot Post</li>
							<li><i class="icon-ok"></i> Advanced Report</li>
							<li><i class="icon-ok"></i> No Watermark</li>
							<li><i class="icon-ok"></i> Event Schedule</li>
							<li><i class="icon-ok"></i> Bulk Schedule</li>
						</ul>
						<div class="plan-cta">
							<a href="#" class="btn btn-large">Order Now</a>
						</div>
					</div><!-- /.plan -->
				</div><!-- /.span4 -->

				<div class="span4">
					<div class="plan">
						<div class="plan-name">
							<h3>Business</h3>
						</div>
						<div class="plan-price">
							<h2>Call</h2>
						</div>
						<ul class="plan-features">
							<li><i class="icon-ok"></i> Unlimited Support <i class="icon-info-sign" data-toggle="tooltip" data-original-title="Jam Kerja"></i></li>
							<li><i class="icon-ok"></i> Unlimited Channel Account <i class="icon-info-sign" data-toggle="tooltip" data-original-title=".com .net .info .biz .org .us .name"></i></li>
							<li><i class="icon-ok"></i> Unlimited Autopilot Post</li>
							<li><i class="icon-ok"></i> Advanced Report</li>
							<li><i class="icon-ok"></i> No Watermark</li>
							<li><i class="icon-ok"></i> Event Schedule</li>
							<li><i class="icon-ok"></i> Bulk Schedule</li>
						</ul>
						<div class="plan-cta">
							<a href="#" class="btn btn-large">Contact Us</a>
						</div>
					</div><!-- /.plan -->
				</div><!-- /.span4 -->
			</div><!-- /.row -->
			<div class="text-center">
			<h5>Not sure about the plan you need? <a href="mailto:info@jarvis-store.com">Contact Us</a></h5>
			<h5><a href="#">Pricing term and condition</a></h5>
		</div>
		</div><!-- /.container -->
		
	</section>
@endsection