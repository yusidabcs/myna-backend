@section('content')
<section class="block-medium">
		<center>
				<h2>Features</h2>
			</center>
			<hr><br><br><br><br>
		<div class="container">
			<div class="row">
				<div class="span5">
					<img src="assets/images/autopilot.jpg">
				</div>
				<div class="span7">
					<h2><i class="icon-thumbs-up"></i> Autopilot Marketing</h2>
					<p>Autopilot marketing memungkinkan barang kita untuk memarketingkan dirinya sendiri secara otomatis. <br><br>Myna akan menganalisa fans, follower anda, trending topic, dan hot trend saat ini sehingga akan menemukan waktu yang tepat untuk mempublish barang anda untuk mendapatkan impact yang besar. <br><br>Tinggal masukkan produk anda sekali saja dan anda bisa bersantai dirumah, tidur, berlibur dan biarkan myna mengurus marketing dan publishing produk anda secara otomatis.</p>
				</div>
			</div>
		</div>
		<hr>
	</section>
	<section class="block-medium">
		<div class="container">
			<div class="row">
				<div class="span7">
					<h2><i class="icon-cogs"></i> Posting Terjadwal</h2>
					<p>Anda ingin mempost produk anda di semua akun anda(facebook, twitter, marketpplace) sekaligus tapi tidak saat ini? Anda bisa menggunakan fasilitas scheduling dari myna. <br><br>Dengan fitur ini anda bisa merencanakan kapan sebuah produk atau post akan dipublish ke akun anda. <br><br>Mungkin besok, bulan depan, tahun depan atau beberapa jam kedepan, semua bisa anda atur dengan myna.</p>
					<br class="spacer-medium">
				</div>
				<div class="span5">
					<img src="assets/images/scheduling.jpg">
				</div>
			</div>
		</div>
		<hr>
	</section>
	<section class="block-medium">
		<div class="container">
			<div class="row">
				<div class="span5">
					<img src="assets/images/routine.jpg">
				</div>
				<div class="span7">
					<h2><i class="icon-eye-open"></i> Posting Rutin</h2>
					<p>Butuh produk anda di posting produk berulang tiap hari? tiap minggu? tiap bulan? atau ketika menjelang event tertentu seperti natal dan tahun baru? Dengan fitur routine publishing, myna bisa membantu anda. <br><br>Cukup sekali memasukkan produk anda, dan atur interval waktu yang anda inginkan maka secara otomatis myna akan melakukan posting berkala untuk produk anda. <br><br>Tidak perlu repot lagi untuk mengepost barang yang sama berkali-kali di facebook atau untuk memberikan ucapan selamat pagi ke follower kita di twitter.</p>
				</div>
			</div>
		</div>
		<hr>
	</section>
	<section class="block-medium">
		<div class="container">
			<div class="row">
				<div class="span7">
					<h2><i class="icon-star"></i> Analisa Akun</h2>
					<p>Ingin tau kapan jam berapa dan hari apa fans dan follower anda sebagian besar online? atau ingin tahu produk yang paling banyak diminati? atau ingin tau siapa follower dan fans yang paling berjasa me-share produk anda? <br><br>Fitur account analyzer dari myna akan membantu anda mendapatkan data-data penting yang bisa anda gunakan sebagai pertimbangan untuk marketing produk anda. Coba dan rasakan sendiri manfaat nyatanya.</p>
					<br class="spacer-medium">
				</div>
				<div class="span5">
					<img src="assets/images/analyzer.jpg">
				</div>
			</div>
		</div>
		<hr>
	</section>
	<section class="block-medium">
		<div class="container">
			<div class="row">
				<div class="span5">
					<img src="assets/images/massscheduling.jpg">
				</div>
				<div class="span7">
					<h2><i class="icon-eye-open"></i> Penjadwal Massal</h2>
					<p>Ingin menjadwal/otomatisasi tweet atau facebook post anda selama sebulan? tiga bulan? atau bahkan jadwal selama satu tahun? <br><br>Fitur Mass Scheduling dari myna dapat membantu anda melakukan itu, sehingga engagement anda dan jadwal posting produk anda bisa diotomatisasi dalam jangka waktu sebulan atau setahun kedepan.</p>
				</div>
			</div>
		</div>
	</section>
@endsection