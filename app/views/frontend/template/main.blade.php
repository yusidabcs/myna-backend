<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>myna.io - Internet marketing automation</title>
	@include('frontend.template.seostuff')
	@include('frontend.template.css')
	<link rel="shortcut icon" href="favicon.png">
</head>

<body>
	@include('frontend.template.navbar')
	<div id='wrapper'>
        @yield('content')
	</div>	
	@include('frontend.template.footer')
	@include('frontend.template.js')	
	<script type="text/javascript">
		$(document).ready(function(){
			$('#register').submit(function(){
				var email = $('#register input[name=email]').val();
				var fn = $('#register input[name=firstName]').val();
				var ln = $('#register input[name=lastName]').val();
				var pw = $('#register input[name=password]').val();
				var pw_c = $('#register input[name=password_confirmation]').val();
				$('.btn-submit').button('loading');
				$.ajax({
				  type: "POST",
				  url: 'http://www.myna.local/register',
				  data: {email:email,firstName:fn,lastName:ln,password:pw,password_confirmation:pw_c},
				  success: function(data,status){
				  	toastr.success(data);
				  	$('#register input').val('');
				  	$('.btn-submit').button('reset');
				  	$('#modal-1').removeClass('md-show');
				  },
				  error: function(e,status){				  					  	
				  	if(e.status===400)
				  	{				  		
				  		var json = $.parseJSON(e.responseText);
				  		json.forEach(function(a) {
						    toastr.error(a);
						});								
				  	}
				  	$('.btn-submit').button('reset');				  	
				  }
				});
				return false;
			});
		});
		$(document).pjax('a', '#wrapper', { fragment: '#wrapper'});
	</script>
</body>
</html>
