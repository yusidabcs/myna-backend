<!-- Footer -->
<div class="md-overlay"></div>
<footer>
	<hr>
	<div class="block-small">
		<div class="container">
			<div class="row">
				<div class="span1 offset2">
					<center><a href="pricing">Pricing</a></center>
				</div>
				<div class="span1">
					<center><a href="feature">Feature</a></center>
				</div>
				<div class="span1">
					<center><a href="#" target="_blank">Blog</a></center>
				</div>
				<div class="span1">
					<center><a href="teams">Team</a></center>
				</div>
				<div class="span1">
					<center><a href="help">Help</a></center>
				</div>
				<div class="span1">
					<center><a href="faq">Faq</a></center>
				</div>
				<div class="span1">
					<center><a href="tos">ToS</a></center>
				</div>
				<div class="span1">
					<center><a href="contact">Contact</a></center>
				</div>
			</div>
		</div>
	</div>
				
	<hr>

	<div class="block-small">
		<div class="container">
			<div class="row">
				<div class="span12">
					<center>
					<p class="rcopy">COPYRIGHT © 2013-2014 PT. Mora Pratama Kreasindo. WEBSITE BUILT WITH ♥ IN BANDUNG &amp; BALI</p>
					</center>
				</div>
			</div>
		</div>
	</div>
</footer>

<!-- Begin LaunchRock Widget -->
<div id="lr-widget" rel="OIMN32M4"></div>
<script type="text/javascript" src="//ignition.launchrock.com/ignition-current.min.js"></script>
<!-- End LaunchRock Widget -->