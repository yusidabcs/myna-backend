<div class="navbar navbar-fixed-top">
	<div class="navbar-inner">
		<div class="container">
			<!-- Toggle button -->
			<a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">Menu</a>
			<!-- Brand Logo -->
			<a class="brand" href="home"><h2>myna.io</h2></a>
			<!-- Navbar links -->
			<nav class="nav-collapse collapse pull-right">
				<ul class="nav">					
					<li><a class="md-trigger" data-modal="modal-1" href="#">Register</a></li>
					<li><a class="md-trigger" data-modal="modal-2" href="#">Login</a></li>
				</ul>
			</nav><!--/.nav-collapse -->
		</div><!-- /.container -->
	</div><!-- /.navbar-inner -->
</div><!-- /.navbar -->
<div class="md-modal md-effect-7" id="modal-1">
	<div class="md-content">
		<h3>Register for Free</h3>
		<div id="register-content">
			{{Form::open(array('url' => 'register','method'=>'POST','id'=>'register'))}}
				<center>
				<input class="input input-block-level" type="email" placeholder="Email" name="email" required>
				<br>
				<input class="input input-block-level" type="text" name="firstName" placeholder="First name.." required>
				<br>
				<input class="input input-block-level" type="text" name="lastName" placeholder="Last name.." required>
				<br>
				<input class="input input-block-level" type="password" name="password" placeholder="Password" required>
				<br>
				<input class="input input-block-level" type="password" name="password_confirmation" placeholder="Retype password" required>
				<br>
				<small>I have read and agree to the <a href="tos">Terms of Use</a></small>
				<br>
				<br>
				<input type="submit" class="btn btn-submit" value="Submit">
				<hr>
				</center>
			</form>
		</div>

	</div>
</div>
<!-- <div class="md-modal md-effect-7" id="modal-2">
	<div class="md-content">
		<h3>Login</h3>
		<div>
			<center>
			<input class="input input-block-level" type="text" placeholder="Email">
			<br>
			<input class="input input-block-level" type="text" placeholder="Password">
			<label class="checkbox" align="left">
		      <input type="checkbox"> Remember Me
		    </label>
			<input type="submit" class="btn" value="Sign In">
			<hr>
			or<br><br>
			<button class="btn btn-info">Login Using Facebook</button>
			<hr>
			<a href="#">Forgot Password ?</a>
			</center>
		</div>

	</div>
</div> -->