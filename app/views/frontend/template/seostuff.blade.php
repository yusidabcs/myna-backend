<meta property="og:title" content="myna.io - Internet marketing, simplified." />
<meta property="og:type" content="company" />
<meta property="og:site_name" content="myna.io - Internet marketing, simplified." />
<meta property="og:description" content="Bayangkan produk yang anda jual bisa memasarkan dirinya sendiri di Internet. Yang anda tinggal lakukan adalah menerima order." />
<meta property="og:url" content="myna.io" />
<meta property="og:image" content="https://launchrock-assets.s3.amazonaws.com/facebook-files/OIMN32M4_1389353560060.jpg?_=0" />

