<?php
use Guzzle\Http\Client;
use App\Transformer\FacebookTransformer;
use App\Transformer\PostTransformer;
class PostsController extends ApiController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index($id)
	{
        if($id==$this->user->id){
			$rs = Post::where('userId','=',$id)->get();
    		return $this->respondWithCollection($rs, new PostTransformer);
		}			
		return $this->errorNotFound();
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
        //return View::make('posts.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	/*return $data;
		array(
			'content',
			'photo',
			'account' => [
				'facebook',
				'facebookpage',
				'twitter'
			],
			'scheduled' => [
				'status'=> 'true/false',
				'date' => 'date',
				'confirmation' => 'true/false'
			],
			'routine' => [				
				'type' => '1,2,3',
				'event' => 'eventId',
				'day' => '1-7 | 0 =for no',
				'date' => '1-31 | 0 = for no',
				'hour' => '0 -23',
				'minute' => '1-60',
				'second' => '1-60',
				'timeType' => 'AM/PM'
			]
		);	*/
	public function store($userId)
	{
		//set timezone	
		date_default_timezone_set($this->user->timeZone);
		
		$rules = array(
			'content'=>'required',
		);
		$validator = Validator::make(Input::all(),$rules);
		if ($validator->fails())
		{
			$messages = $validator->messages();
			return $this->errorWrongArgs($messages->all());			
		}		
		$data = Input::all();		
		
		//save post
		$post = new Post;
		$post->content = $data['content'];
		$post->photo = $data['photo'];
		$post->routine = '';
		$post->status = 0;
		$post->userId = $userId;
		$post->save();

		//save transaction
		$post->facebooks()->sync(Input::get('account.facebook'));
		//$post->facebookpages()->sync(Input::get('account.facebookpage'));
		//$post->twitters()->sync(Input::get('account.twitter'));
		//save schedule
		if($data['scheduled']['status']==true)
		{						
			//save schedule
			$scheduled = new Scheduledpush;
			$scheduled->pushDate = $data['scheduled']['date'];
			$scheduled->status = 0;
			$scheduled->note = '';
			$scheduled->confirmation = $data['scheduled']['confirmation'];			
			$scheduled->userId = $userId;
			$scheduled->postId = $post->id;
			$scheduled->save();

			$now = date('r');
			$now = strtotime($now);
			$time = strtotime(Input::get('scheduled.date'));
			
			$now = (int)$now;
			$future = (int)$time;
			$start_at = ($future-$now);
			$worker = new IronWorker(Config::get('iron'));			
			$pushId = $worker->postScheduleSimple('ScheduleWorker',array(
				'postId'=>$post->id,
				'scheduledId'=>$scheduled->id,
				'authorization'=>Request::header('Authorization')
				), $start_at);

			$scheduled->pushId = $pushId;
			$scheduled->save();
		}
		else if($data['routine']['type']!=0)
		{
			//save routine
			$routine = new Routinepush;
			$routine->date = Input::get('routine.date');
			$routine->day = Input::get('routine.day');
			$routine->time = Input::get('routine.time');
			$routine->status = 1;
			$routine->userId = $userId;
			$routine->postId = $post->id;
			$routine->save();

			//jika harian schedule post langsung pertama x
			if($routine->date==0 || $routine->day==0)
			{
				$scheduled = new Scheduledpush;
				$scheduled->pushDate = date('Y-m-d '.$routine->time);
				$scheduled->status = 0;
				$scheduled->note = 'schedule from routine.';
				$scheduled->confirmation = 0;
				$scheduled->userId = $userId;
				$scheduled->postId = $post->id;
				$scheduled->save();
				$now = date('r');
				$now = strtotime($now);
				$time = strtotime($scheduled->pushDate);			;
				$now = (int)$now;
				$future = (int)$time;
				$start_at = ($future-$now);
				$worker = new IronWorker(Config::get('iron'));
				$pushId = $worker->postScheduleSimple('ScheduleWorker',array(
					'postId'=>$post->id,
					'scheduledId'=>$scheduled->id,
					'authorization'=>Request::header('Authorization')
					), $start_at);

				$scheduled->pushId = $pushId;
				$scheduled->save();
			}			
		}
		else
		{
			//queue status normaly
			$data = Request::header('Authorization');		
			Queue::push(function($job) use ($post,$data)
			{				
				$header = array(
					'Authorization' => $data
				);			
				$client = new Client(Config::get('app.url'));
				$request = $client->get('queue/posts/'.$post->id,$header,array('scheduledId'=>0));
				$response = $request->send();		
				$job->delete();
			});	
		}		
        return $this->respondWithItem($post, new PostTransformer);
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{  					
  		$client = new Client('http://mycode.eu1.frbit.net');
		//Queue::push(function($job) use ($data,$post,$client) 
		//{				  
		$request = $client->get('queue/post/1');
		$response = $request->send();

		return $response;  			
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
        return View::make('posts.edit');
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}
