<?php
use App\Transformer\UserTransformer;
class UsersController extends ApiController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$user = User::find(Auth::user()->id);
        return $this->respondWithItem($user, new UserTransformer);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
        //return View::make('users.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
        //return View::make('users.show');
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
        //return View::make('users.edit');
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$rules = array(
			'firstName' => 'required|max:200',
			'lastName' => 'required|max:200',
			'email' => 'required|email|unique:users,email,'.$id,
			'emailNotification' => 'required|email|unique:users,emailNotification,'.$id,
			'timeZone' => 'required',
			'currency' => 'required',
			'language' => 'required'
			);
		if(Input::get('password')){
			$rules = array_add($rules, 'password', 'required|min:6|confirmed');
		}
		$validator = Validator::make(Input::all(),$rules);

		if ($validator->fails())
		{
		    $messages = $validator->messages();
			return $this->errorWrongArgs($messages->all());
		}

		$user = User::find($id);
		$user->firstName = Input::get('firstName');
		$user->lastName = Input::get('lastName');
		$user->email = Input::get('email');
		$user->timeZone = Input::get('timeZone');
		$user->currency = Input::get('currency');
		$user->language = Input::get('language');
		$user->schedulePostNotif = Input::get('schedulePostNotif');
		$user->routinePostNotif = Input::get('routinePostNotif');
		$user->autopilotPostNotif = Input::get('autopilotPostNotif');
		if(Input::get('password'))
		{
			$user->password = Hash::make(Input::get('password'));
		}
		$user->save();
		return $this->respondWithItem($user, new UserTransformer);
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}
