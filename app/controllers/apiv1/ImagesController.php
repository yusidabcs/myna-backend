<?php
use App\Transformer\ImageTransformer;
use Illuminate\Filesystem\Filesystem;
class ImagesController extends ApiController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index($id)
	{
		if($id==$this->user->id){
			$rs = Images::where('userId','=',$id)->get();
    		return $this->respondWithCollection($rs, new ImageTransformer);
		}			
		return $this->errorNotFound();        
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
        //return View::make('images.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store($id)
	{
		$file = Input::file('file');
        $files = new Filesystem;
        $destinationPath = 'images/';
        $destinationPath2 = 'images/thumb/';        
        $filename = date('Ymd-His-').$this->user->id;
        $extension =$file->getClientOriginalExtension(); //if you need extension of the file
        $img1 = Image::make($file->getRealPath())->resize(960,null,true);
        $img2 = Image::make($file->getRealPath())->grab(160);        
        $img1->save($destinationPath.$filename.'.'.$extension);
        $img2->save($destinationPath2.$filename.'.'.$extension);

        $img = new Images;
        $img->image = Config::get('app.base_url').'/images/'.$filename.'.'.$extension;
        $img->thumb = Config::get('app.base_url').'/images/thumb/'.$filename.'.'.$extension;
        $img->userId = $this->user->id;
        $img->save();
       	return $this->respondWithItem($img, new ImageTransformer);
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
        $rs = Images::find($id)->toArray();
        return Response::json($rs,200);
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
        return View::make('images.edit');
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($userId,$id)
	{
		$img = Images::find($id);
		$path = pathinfo($img->image);
		$base = $path['basename'];
		File::delete(public_path().'/images/'.$base);
		File::delete(public_path().'/images/thumb/'.$base);
		$img->delete();
		return $this->respondWithItem($img, new ImageTransformer);
	}

}
