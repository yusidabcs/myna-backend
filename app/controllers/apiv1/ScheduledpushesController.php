<?php
use App\Transformer\ScheduleTransformer;
use League\Fractal\Cursor\Cursor;
use League\Fractal\Resource\Collection;
use League\Fractal\Pagination\IlluminatePaginatorAdapter;
class ScheduledpushesController extends ApiController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index($id)
	{
		$type = Input::get('type') ? Input::get('type') : 'paginator';
		$per_page = Input::get('number') ? (int) Input::get('number') : 20;
		
		if($type=='paginator')
		{			
			$paginator = Scheduledpush::where('userId','=',$id)
				->paginate($per_page);

			$places = $paginator->getCollection();
			
			$resource = new Collection($places, new ScheduleTransformer);
			$resource->setPaginator(new IlluminatePaginatorAdapter($paginator));

			return $this->respondWithCursor($resource);	
		}		
		elseif ($type=='cursor') 
		{
			$current = Input::get('cursor')==NULL ? (int) base64_decode(Input::get('cursor')) : 0;						
			$places = Scheduledpush::where('userId','=',$id)
				->limit($per_page)
				->skip($current)
				->get();
			$next = base64_encode((string) ($current + $per_page));
			$cursor = new Cursor($current, $next, $places->count());
			$resource = new Collection($places, new ScheduleTransformer);
			$resource->setCursor($cursor);

			return $this->respondWithCursor($resource);		
		}
		
        return $this->errorNotFound();
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
        //
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
        //
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
        //
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($userId,$id)
	{	
		/*[		
		'date' =>
		'note' => 
		'status' => 0,1,2
		'note' => 
		'confirmation'=>
		]*/		
		//set timezone	
		date_default_timezone_set($this->user->timeZone);
		$scheduled = Scheduledpush::find($id);
		//cancel current schedule
		$worker = new IronWorker(Config::get('iron'));			
		if($scheduled->pushDate!=Input::get('pushDate')){
			$response = $worker->deleteSchedule($scheduled->pushId);

			$now = date('r');
			$now = strtotime($now);
			$time = strtotime(Input::get('pushDate'));
			
			$now = (int)$now;
			$future = (int)$time;
			$start_at = ($future-$now);
			$pushId = $worker->postScheduleSimple('ScheduleWorker',array(
				'postId'=>$scheduled->postId,
				'scheduledId'=>$scheduled->id,
				'authorization'=>Request::header('Authorization')
				), $start_at);
			$scheduled->pushId = $pushId;
		}
		
		//update schedule		
		$scheduled->pushDate = Input::get('pushDate');
		$scheduled->status = Input::get('status');
		$scheduled->note = Input::get('note');
		$scheduled->confirmation = Input::get('confirmation');			
		$scheduled->save();

		return $this->respondWithItem($scheduled, new ScheduleTransformer);

	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($userId,$id)
	{
		//delete schedule
		$schedule = Scheduledpush::find($id);		
		
		//delete schedule di iron worker
		$worker = new IronWorker(Config::get('iron'));			
		$response = $worker->deleteSchedule($schedule->pushId);
		if($response->msg=='Cancelled'){
			$schedule->delete();
		}
		return $this->respondWithItem($schedule, new ScheduleTransformer);
	}

}
