<?php
use Guzzle\Http\Client;
use App\Transformer\FacebookTransformer;

class FacebooksController extends ApiController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index($id)
	{
		if($id==$this->user->id){
			$rs = Facebook::where('userId','=',$id)->get();
    		return $this->respondWithCollection($rs, new FacebookTransformer);
		}			
		return $this->errorNotFound();
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
        //return View::make('facebooks.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$validator = Validator::make(
			Input::all(),
		    array('fbUserId' => 'unique:facebooks,fbUserId'),
		    array(
			    'fbUserId.unique' => 'The Facebook Akun is already added.',
			)
		);
		if ($validator->fails())
		{
			$messages = $validator->messages();
			return $this->errorWrongArgs($messages->first('fbUserId'));			
		}		
		$client = new Client('https://graph.facebook.com');
		//extend long term token access fb
		$request = $client->get('oauth/access_token?grant_type=fb_exchange_token&client_id='.Config::get('social.Facebook.appId').'&client_secret='.Config::get('social.Facebook.appSecret').'&fb_exchange_token='.Input::get('fbUserAccessToken'));
		$response = $request->send();
		$a= $response->getBody();
		$rs = array();
		parse_str($a, $rs);
		//end long term token fb
		//get username fb		
		$request2 = $client->get(Input::get('fbUserId'));
		$response2 = $request2->send();
		$rs2= $response2->getBody();	
		$rs2 = json_decode($rs2);		
		//end

		$fb = new Facebook;
		$fb->fbUsername = $rs2->name;
		$fb->fbUserId = Input::get('fbUserId');
		$fb->fbUserAccessToken = $rs['access_token'];
		$fb->userId = $this->user->id;
		$fb->save();
        return $this->respondWithItem($fb, new FacebookTransformer);
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id,$id2)
	{
		$fb = Facebook::find($id2);
		return $this->respondWithItem($fb, new FacebookTransformer);
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
        //return View::make('facebooks.edit');
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id,$id2)
	{		
		$fb = Facebook::where('userId','=',$id)->find($id2);
		if($fb){
			Facebook::destroy($id2);
			return $this->respondWithItem($fb, new FacebookTransformer);
		}else{
			return $this->errorNotFound();
		}
	}

}
