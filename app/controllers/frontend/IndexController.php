<?php
namespace Frontend;

class IndexController extends BaseController
{

    public function index()
    {

        $this->layout->content = \View::make('frontend.home');
    }
}