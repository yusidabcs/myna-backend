<?php
namespace Frontend;

class PricingController extends BaseController
{

    public function index()
    {
    	if(\Request::header('X-PJAX'))
    	{	
    		return \View::make('frontend.pricing');
    	}
        $this->layout->content = \View::make('frontend.pricing');
    }
}