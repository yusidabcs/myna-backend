<?php
namespace Frontend;
class RegisterController extends \Controller {

	/**
	 * Setup the layout used by the controller.
	 *
	 * @return void
	 */	
	protected function index()
	{
		$rules = array(
			'email'=>'required|email|unique:users,email',
			'firstName' => 'required',
			'lastName' => 'required',
			'password' => 'required|min:6|confirmed',
			);
		$validator = \Validator::make(\Input::all(),$rules);
		if ($validator->fails())
		{
		    $messages = $validator->messages();
		    return \Response::json($messages->all(),400);
		}

		$user = new \User;
		$user->email = \Input::get('email');
		$user->password = \Hash::make(\Input::get('password'));
		$user->firstName = \Hash::make(\Input::get('firstName'));
		$user->lastName = \Input::get('lastName');
		$user->save();	        
	    return \Response::json(array('Successfull add new member. Please check your email for detail informations'),200);
	}

}