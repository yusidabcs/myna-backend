<?php
namespace Frontend;

class TeamController extends BaseController
{

    public function index()
    {

        $this->layout->content = \View::make('frontend.teams');
    }
}