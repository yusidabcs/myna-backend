<?php
namespace Frontend;

class FaqController extends BaseController
{

    public function index()
    {

        $this->layout->content = \View::make('frontend.faq');
    }
}