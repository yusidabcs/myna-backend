<?php
namespace Frontend;
use \View;
class BaseController extends \Controller {

	/**
	 * Setup the layout used by the controller.
	 *
	 * @return void
	 */
	public $layout = 'frontend.template.main';
	protected function setupLayout()
	{
		$this->layout = View::make($this->layout);
	}

}