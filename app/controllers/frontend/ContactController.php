<?php
namespace Frontend;

class ContactController extends BaseController
{

    public function index()
    {

        $this->layout->content = \View::make('frontend.contact');
    }
}