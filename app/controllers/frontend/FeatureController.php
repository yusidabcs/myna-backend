<?php
namespace Frontend;

class FeatureController extends BaseController
{

    public function index()
    {

        $this->layout->content = \View::make('frontend.feature');
    }
}